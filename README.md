# Posts && Comments Demo App

This is a demo app which displays posts and their comments to logged in users.

## Running The App

After running `npm i` in the project dir, you can start the app in development mode with

```
npm run start
```

To make a production build, type

```
npm run build
```

## Using The App

There are three pages:

1. the login page
2. the posts page
3. the post details page

### Login Page

When not logged in, this is the only page you can see.
To log in, keep both fields empty and click _Log In_. Any other input will result in a failed login.

### Posts Page

There's a list of posts on this page. Each post has its comments which can be seen by clicking on the _Comments_ link.

You can search through the posts using the input on top of the list. It will filter posts based on the user who wrote it.

Clicking on the post title will open the post in the post details page.

### Post Details Page

This page shows the post you clicked on and its comments. That is all.

Click _Back_ to go back to the list.

### Logging Out

There's an avatar in the top right corner and clicking it opens a dropdown with a single option: _Log out_.

## Greetings

If you open the dev tools console, you'll see every rendered component greet you with

```
Hello, my name is <component name>
```

Two things needed to be done to accomplish this:

1. attach a prop to all the components `{ greeting: "Hello, my name is" }`
2. make every component add its name to the message and display it when it renders

Both those things were done by monkey patching the `React.createElement` method.

The first patch is applied by calling `applyGreetingToAll(greeting)` method found in `src/greeting.js`.

The second, a bit more complex, is implemented in `src/renderLog.js`. The render log can be initialized by calling the `apply(logger)` method where `logger` is a function which accepts component `props` and `displayName`. It is called by each component as it renders.

The render log covers all components used in the app. There may be some which are not covered.

## Notes

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
Apart from what Create React App has installed, this app employs:

* a `jsoncofig.json` file to enable [absolute imports](https://create-react-app.dev/docs/importing-a-component#absolute-imports)
* [React Router](https://reacttraining.com/react-router/web/) for routing
* [React Bootstrap](https://react-bootstrap.github.io) for a nice GUI
* [node-sass](https://github.com/sass/node-sass) to enable theming

## Things Left To Be Done

* the app hasn't been debugged thoroughly; there are probably still some edge cases in which unexpected stuff happens, but it _seems_ to work fine :)
* not all components have explicitly set display names which is why the production build shows stupid things in their greetings
* greetings should probably be turned off in the production build anyway
* remove unnecessary boilerplate left over from create-react-app
* tests :/
