import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";

import Avatar from "Components/Avatar";
import LoginPage from "Components/LoginPage";
import PostsPage from "Components/PostsPage";
import PostDetailsPage from "Components/PostDetailsPage";
import "./App.css";

import { useStoreToState } from "Store";

// routes used when a user is logged in
const AppRoutes = () => (
    <Router>
        <Switch>
            <Route exact path="/app">
                <PostsPage />
            </Route>
            <Route exact path="/post/:postId">
                <PostDetailsPage />
            </Route>
            <Route path="*">
                <Redirect to="/app" />
            </Route>
        </Switch>
    </Router>
);

function App() {
    const user = useStoreToState("currentUser");

    // an unauthorized user can only see the login page
    return user ?
        <><AppRoutes /><Avatar /></> :
        <LoginPage />
}

export default App;
