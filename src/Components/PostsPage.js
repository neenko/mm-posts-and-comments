import React from "react";

import withUsersAndPosts from "Components/withUsersAndPosts";
import { byProp } from "utils";

import PostsList from "Components/PostsList";

function PostsPage({ users, posts }) {

    // a function which filters posts by user name
    const getPosts = (searchTerm) => posts.filter((post) => {
            if (!searchTerm) return true;

            const user = byProp(users, "id", post.userId);
            return user.name.toLowerCase()
                .indexOf(searchTerm.toLowerCase()) > -1;
        });

    return(<div className="content-page posts-page">
        <h1>Posts &amp;&amp; Comments</h1>
        <PostsList getPosts={getPosts} users={users}/>
    </div>);
};

// the posts page depends on users and posts data being in the store
export default withUsersAndPosts()(PostsPage);