
import React from "react";
import PropTypes from "prop-types";
import ListGroup from "react-bootstrap/ListGroup";

const propTypes = {
    comments: PropTypes.array.isRequired,
    getUserByEmail: PropTypes.func.isRequired
};

// displays a list of comments for a post
function CommentList({ comments, getUserByEmail }) {
    return (
        <ListGroup variant="flush">{comments.map((comment) => {
            // try to get the user who wrote the comment;
            // default to their email if no users were found
            const user = getUserByEmail(comment.email);
            const userName = user ? user.name : comment.email;

            return (
                <ListGroup.Item key={comment.id}>
                    <div className="commment-title">
                        {`${userName}: `}
                        <strong>{comment.name}</strong>
                    </div>
                    <div>{comment.body}</div>
                </ListGroup.Item>
            );
        })}</ListGroup>
    );
};

CommentList.propTypes = propTypes;

export default CommentList;