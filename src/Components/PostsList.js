import React, { useState } from "react";
import PropTypes from "prop-types";
import Accordion from "react-bootstrap/Accordion";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";

import { byProp } from "utils";

import Post from "Components/Post";
import CommentList from "Components/CommentList";

const propTypes = {
    getPosts: PropTypes.func.isRequired,
    users: PropTypes.array
}

function PostsList({ getPosts, users }) {
    const [searchTerm, setSearchTerm] = useState("");

    // posts filter change handler
    const handleSearch = (evt) => {
        setSearchTerm(evt.target.value);
    }

    // functions to look through the store to find users by email and id
    const getUserByEmail = (email) => byProp(users, "email", email);
    const getUserById = (userId) => byProp(users, "id", userId);

    // get (filtered) posts
    const posts = getPosts(searchTerm);

    return (<>
        <Form.Control as="input" type="search" className="posts-search"
            placeholder="Filter posts by user"
            value={searchTerm}
            onChange={handleSearch} />

        <small className="nr-of-posts text-muted">{
            searchTerm ? `Found ${posts.length} posts` : `${posts.length} posts`
        }</small>

        <Accordion>{
            posts.map(post =>
                <div className="post" key={post.id}>
                    <div>
                        <Post post={post} user={getUserById(post.userId)} />
                        <Accordion.Toggle as={Button} variant="link" eventKey={post.id}>
                            {`Comments (${post.comments.length})`}
                        </Accordion.Toggle>
                    </div>
                    <Accordion.Collapse eventKey={post.id}>
                        <div>
                            <CommentList
                                comments={post.comments}
                                getUserByEmail={getUserByEmail}
                            />
                        </div>
                    </Accordion.Collapse>
                </div>)
        }</Accordion>
    </>);
}

PostsList.propTypes = propTypes;

export default PostsList;