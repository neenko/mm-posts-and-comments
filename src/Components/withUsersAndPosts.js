import React, { useEffect } from "react";
import Spinner from "react-bootstrap/Spinner";

import { useStoreToState } from "Store";
import userService from "Services/userService";
import postsAndCommentsService from "Services/postsAndCommentsService";

// provide a hoc which provides wrapped components with users and posts data
const wrapComponent = (component) => (props) => {
    const users = useStoreToState("users");
    const posts = useStoreToState("posts");

    useEffect(() => {
        !users && userService.getUsers();
        !posts && postsAndCommentsService.getPostsAndComments();
    });

    if (!users || !posts) {
        return (
            <Spinner className="loader" animation="border" role="status">
                <span className="sr-only">Loading...</span>
            </Spinner>
        );
    }

    return component({ users, posts, ...props });
};

export default () => {
    return (component) => {
        const wrapped = wrapComponent(component);
        wrapped.displayName = "withUsersAndPosts";

        return wrapped;
    };
};