import React from "react";
import { Link, useParams } from "react-router-dom";
import Alert from "react-bootstrap/Alert";

import withUsersAndPosts from "Components/withUsersAndPosts";
import { byProp } from "utils";

import Post from "Components/Post";
import CommentList from "Components/CommentList";

function PostDetailsPage({ users, posts }) {
    const { postId } = useParams();
    const post = byProp(posts, "id", parseInt(postId));

    // if the postId doesn't belong to any post, show this message
    if (!post) {
        return (<div className="text-center no-post">
            <Alert variant="secondary">This post isn't available.</Alert>
            <Link to="/app">Go back to the list</Link>
        </div>);
    }

    return (<div className="content-page post-details-page">
        <Link className="go-back" to="/app">Back to list</Link>
        <Post
            post={post}
            user={byProp(users, "id", post.userId)}
        />
        <CommentList
            comments={post.comments}
            getUserByEmail={(email) => byProp(users, "email", email)}
        />
    </div>);
};

// the post details page depends on users and posts data being in the store
export default withUsersAndPosts()(PostDetailsPage);