import React, { useState } from "react";
import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";
import userService from "Services/userService";

export default function Avatar() {
    const [goHome, setGoHome] = useState(false);

    const handleLogout = () => {
        userService.logout();
        setGoHome(true);
    }

    if (goHome) {
        // after logout, the app should return to the root and show
        // the login screen
        document.location.replace("/");
        return null;
    }
    return (
        <DropdownButton className="avatar-button" title="MM" size="large">
            <Dropdown.Item onClick={handleLogout}>Logout</Dropdown.Item>
        </DropdownButton>
    );
};