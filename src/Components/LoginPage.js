import React, { useState } from "react";
import Card from "react-bootstrap/Card";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Spinner from "react-bootstrap/Spinner";
import userService from "Services/userService";
import pic from "mm-posts-and-comments.png";

const LoginText = ({ loginFailed, children }) =>
    <span className={loginFailed ? "login-failed" : ""}>{children}</span>;

export default function LoginPage() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [loginFailed, setLoginFailed] = useState(false);
    const [loginInProgress, setLoginInProgress] = useState(false);

    async function handleSubmit(evt) {
        evt.preventDefault();

        setLoginInProgress(true);

        try {
            await userService.login(
                email,
                password,
                () => setLoginFailed(false));
        } catch (err) {
            setLoginFailed(true);
            setLoginInProgress(false);
        }
    }

    return (
        <Form className="login-form" onSubmit={handleSubmit}>
            <Card>
                <Card.Img variant="top" src={pic} />
                <Card.Body>
                    <Card.Title>
                    <LoginText {...{ loginFailed }}>
                        {loginFailed ? "Log in failed. Try again." : "Log in"}
                    </LoginText>
                    </Card.Title>
                    <Form.Group controlId="formGroupEmail">
                        <Form.Label>Email</Form.Label>
                        <Form.Control
                            type="email"
                            placeholder="Email"
                            disabled={loginInProgress}
                            autoFocus
                            value={email}
                            onChange={evt => setEmail(evt.target.value)}
                        />
                    </Form.Group>
                    <Form.Group controlId="formGroupPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            type="password"
                            autoComplete="type-password"
                            placeholder="Password"
                            disabled={loginInProgress}
                            value={password}
                            onChange={evt => setPassword(evt.target.value)}
                        />
                    </Form.Group>
                    <Card.Text>
                        <small className="text-muted">
                            <LoginText {...{ loginFailed }}>Leave both fields blank ;)</LoginText>
                        </small>
                    </Card.Text>
                </Card.Body>
                <Card.Footer>
                    <Button disabled={loginInProgress} variant="primary" type="submit">Log in</Button>
                    { loginInProgress ?
                        <Spinner className="loader login-loader" animation="border" role="status">
                            <span className="sr-only">Please wait...</span>
                        </Spinner>  : null}
                </Card.Footer>
            </Card>
        </Form>
    );
};