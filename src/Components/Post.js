import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const propTypes = {
    post: PropTypes.shape({
        id: PropTypes.any.isRequired,
        title: PropTypes.string.isRequired,
        body: PropTypes.string.isRequired
    }),
    user: PropTypes.shape({
        name: PropTypes.string.isRequired
    })
};

function Post({ post, user }) {
    return (<>
        <h4>
            <Link to={`/post/${post.id}`}>{post.title}</Link>
            <small className="text-muted">{` by ${user.name}`}</small>
        </h4>
        <p className="post-body">{post.body}</p>
    </>);
};

Post.propTypes = propTypes;

export default Post;