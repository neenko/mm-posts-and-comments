const memoCache = Symbol("Memoization cache");

// returns an object from an array based on a prop value
function byProp(arr, prop, val) {
    // to avoid searching for the same array element again and again,
    // this function uses a memoization cache stored on the array itself
    if (!arr[memoCache]) {
        // create memoization cache
        arr[memoCache] = {};
    }

    if (!arr[memoCache][prop]) {
        // prepare a slot for this prop
        arr[memoCache][prop] = {};
    }

    // return from cache if available
    if (arr[memoCache][prop][val]) {
        return arr[memoCache][prop][val];
    }

    const result = arr.find(element => element[prop] && element[prop] === val);

    // save to memo
    arr[memoCache][prop][val] = result;

    return result;
}

export { byProp };