import React from "react";

import renderLog from "renderLog";

export default {
    applyGreetingToAll: (greeting) => {
        // override React.createElement to add the `greeting` prop to all components
        React.createElement = ((createElement) => {
            return (
                function createElementWithGreeting(type, props, ...children) {
                    // don't add props to fragments
                    if (typeof type === "symbol" && type.description === "react.fragment") {
                        return createElement.call(React, type, props, ...children);
                    };

                    const propsWithGreeting = Object.assign({}, props || {}, { greeting });

                    return createElement.call(React, type, propsWithGreeting, ...children);
                }
            );
        })(React.createElement);
    },

    showGreeting: () => {
        // user renderLog to display greeting messages
        renderLog.apply((props, displayName) =>
            props.greeting && console.log(`${props.greeting} ${displayName}`));
    }
};