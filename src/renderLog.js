import React, { useEffect } from 'react';

const patchClassComponent = (Component, displayName, loggingFunc) => {
    // extend the incoming class component with just a render method
    class Patched extends Component {
        render() {
            loggingFunc(this.props, displayName);
            return super.render ? super.render() : null;
        }
    }

    Patched.displayName = displayName;

    return Patched;
};

// since a functional component can't normally accept a ref we need this helper
// which is used with React.forwardRef to pass a ref to the wrapped component
const createFunctionalWithRef = (type, displayName, originalCreateElement, loggingFunc) =>
    ({ forwardedRef, ...props }) => {
        useEffect(() => loggingFunc(props, displayName));

        // extend props with a forwarded ref
        const propsWithRef = forwardedRef ? Object.assign({}, props, { ref: forwardedRef }) : props;

        return originalCreateElement.apply(React, [type, propsWithRef]);
    };

// this is used to patch builtin components such as `a`, `div` etc. but also other
// components which can have a ref prop
const patchBuiltinComponent = (type, displayName, originalCreateElement, loggingFunc) => {
    // prepare a functional component with a patch
    const patched = createFunctionalWithRef(type, displayName, originalCreateElement, loggingFunc);

    patched.displayName = displayName;

    // forward refs to the original component
    return React.forwardRef((props, ref) => {
        const propsWithRef = ref ? Object.assign({}, props, { forwardedRef: ref }) : props;

        return originalCreateElement.apply(React, [patched, propsWithRef]);
    });
}

// this patches functional components, simple
const patchFunctionalComponent = (type, displayName, originalCreateElement, loggingFunc) =>
    (props) => {
        useEffect(() => loggingFunc(props, displayName));
        return originalCreateElement.apply(React, [type, props]);
    };

// use the patch for builtin components to patch 'others': forwardRefs, providers, etc.
const patchOtherComponent = (type, displayName, originalCreateElement, loggingFunc) => {
    const patched = patchBuiltinComponent(type, displayName, originalCreateElement, loggingFunc);

    return patched;
}

// provides a patch for components based on their type: builtin, functional etc.
const patchComponent = (type, displayName, originalCreateElement, loggingFunc) => {
    // skip fragments; they don't have the `greeting` prop anyway
    if (type === Symbol.for("react.fragment")) {
        return type;
    }

    // class components
    if (type.prototype && !!type.prototype.isReactComponent) {
        return patchClassComponent(type, displayName, loggingFunc);
    }

    // builtins
    if (typeof type === "string") {
        return patchBuiltinComponent(type, displayName, originalCreateElement, loggingFunc);
    }

    // functional components
    if (typeof type === "function") {
        return patchFunctionalComponent(type, displayName, originalCreateElement, loggingFunc);
    }

    // 'other'
    if (type.$$typeof === Symbol.for("react.forward_ref") ||
        type.$$typeof === Symbol.for("react.provider") ||
        type.$$typeof === Symbol.for("react.context")) {
        return patchOtherComponent(type, displayName, originalCreateElement, loggingFunc);
    }

    // in case we run into a component type we don't yet handle, throw an error
    throw new Error("Component type not implemented", { type, displayName });
}

// retrieve a patched component either from memo or create a new patch
const getPatchedComponent = (patchedComponents, type, displayName, originalCreateElement, loggingFunc) => {
    if (patchedComponents.has(type)) {
        // get from memo
        return patchedComponents.get(type);
    }

    // create a new patch for this type
    const patched = patchComponent(type, displayName, originalCreateElement, loggingFunc);
    // save to memo
    patchedComponents.set(type, patched);

    return patched;
};

// tries to find a components name to display
const getDisplayName = (type) =>
    (type.displayName || type.name) ||
    (type.type && getDisplayName(type.type)) ||
    (type.render && getDisplayName(type.render)) ||
    (typeof type === "string" && type) ||
    (typeof type.$$typeof === "symbol" && type.$$typeof.description);

// this is a factory function which inits the monkey patch
const apply = (React, createElement, createFactory, loggingFunc) => {
    // save original functions
    const originalCreateElement = createElement;
    const originalCreateFactory = createFactory;

    // prepare a memo object
    const patchedComponents = new Map();

    // patch createElement
    React.createElement = function (type, ...rest) {
        const displayName = getDisplayName(type) || "???";

        try {
            // patch the current component
            const patchedComponent = getPatchedComponent(
                patchedComponents, type, displayName, originalCreateElement, loggingFunc);

            // use original createElement on the patched component
            return originalCreateElement.apply(React, [patchedComponent, ...rest]);
        } catch (err) {
            // this should only happen if we run into a component type not yet supported
            console.error("failed render log", { err, displayName, type });
        }

        // default if an error happens so the app doesn't break
        return originalCreateElement.apply(React, [type, ...rest]);
    };

    Object.assign(React.createElement, originalCreateElement);

    // patch createFactory to use the patched createElement
    React.createFactory = (type) => {
        const factory = React.createElement.bind(null, type);
        factory.type = type;
        return factory;
    }

    Object.assign(React.createFactory, originalCreateFactory);

};

export default {
    apply: (loggingFunc) => apply(React, React.createElement, React.createFactory, loggingFunc)
};