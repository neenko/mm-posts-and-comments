import { set, reset as resetStore } from "Store";
import baseService from "Services/baseService";

const mockUserData = {
    token: "bWFydGlhbmFuZG1hY2hpbmU="
};

export default {
    login: (email, password, beforeSet = null) => {
        return new Promise((resolve, reject) => {
            // act like something is happening for a second
            setTimeout(() => {
                // allow login only if email and password are empty
                if (!email && !password) {
                    resolve();
                    // keep user data in localStorage
                    localStorage.setItem("currentUser", JSON.stringify(mockUserData));
                    // call the beforeSet handler if there is one
                    beforeSet && beforeSet(mockUserData);
                    // add mock user data to the store
                    set("currentUser", mockUserData);
                } else {
                    reject();
                }
            }, 1000);
        });
    },

    logout: () => {
        // remove all stored data
        localStorage.removeItem("currentUser");
        resetStore();
    },

    getUsers: async function() {
        try {
            // fetch users from backend
            const data = await baseService.get("users");
            // save to store
            return set("users", data);
        } catch (err) {
            set("users", null);
            return Promise.reject(err);
        }
    }
};