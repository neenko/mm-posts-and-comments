import { get as storeGet } from "Store";

const baseUrl = "https://demo.martian.agency/api";

const getToken = () => {
    const userData = storeGet("currentUser");

    return userData ? userData.token : null;
}

export default {
    get: async function (resource) {
        const url = `${baseUrl}/${resource}`;

        const options = {};
        const token = getToken();

        if (token) {
            options.headers = { "X-Auth": token };
        }

        try {
            const response = await fetch(url, options)
            return response.json();
        } catch (err) {
            return Promise.reject(err);
        }
    }
}