import { set } from "Store";
import baseService from "Services/baseService";

export default {
    getPostsAndComments: async function() {
        try {
            // fetch all posts and comments from backend
            const [postsData, commentsData] = await Promise.all(
                [baseService.get("posts"), baseService.get("comments")]);

            // add comments to posts
            const posts = postsData.map(post => Object.assign(post, {
                comments: commentsData.filter(comment => comment.postId === post.id)
            }));

            // store posts data
            set("posts", posts);
            return posts;
        } catch (err) {
            set("posts", null);
            return Promise.reject(err);
        }
    }
};