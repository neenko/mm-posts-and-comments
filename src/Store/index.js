import { useEffect, useState } from "react";

// tries to get persisted user data from local storage
const getUserFromLocalStorage = () => {
    try {
        return JSON.parse(localStorage.getItem("currentUser"));
    } catch (err) {
        return null;
    }
}

// this is used both to init and reset the store
const initStore = () => [
    {
        currentUser: getUserFromLocalStorage(),
        users: null,
        posts: null
    },
    {}
]

// the initial store state
let [store, subscriptions] = initStore();

// used to attach a handler function for store prop ('channel') changes
const subscribeTo = (channel, handler) => {
    if (typeof handler !== "function") {
        return;
    }

    if (!subscriptions[channel]) {
        subscriptions[channel] = [];
    }

    subscriptions[channel].push(handler);

    // the unsubscribe function
    return () => {
        subscriptions[channel] = subscriptions[channel].filter(func => func !== handler);
    }
};

// calls all handler functions for a certain prop/channel
const broadcast = channel => {
    const handlers = subscriptions[channel];

    if (handlers && handlers.length) {
        handlers.forEach(func => func.call(null, store[channel]));
    }
};

// clones most js objects
const clone = val => val === undefined ? undefined : JSON.parse(JSON.stringify(val));

// sets a store property and broadcasts the change
const set = (prop, value) => {
    store[prop] = clone(value);
    broadcast(prop);

    return store[prop];
}

// returns a store prop value
const get = (prop) => store[prop];

const reset = () => {
    [store, subscriptions] = initStore();
};

// hooks into store changes and saves the change to component state
const useStoreToState = (prop) => {
    if (!store.hasOwnProperty(prop)) {
        throw new Error(`Unknown store prop ${prop}`);
    }

    const [state, setState] = useState(store[prop]);

    // the subscribeTo function will return a function which unsubscribes;
    // this cleans up after a component unmounts
    useEffect(() => subscribeTo(prop, setState), [prop, state]);

    return state;
};

export { get, set, reset, useStoreToState };